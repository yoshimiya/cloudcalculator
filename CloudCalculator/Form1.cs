﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CloudCalculator
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }
        string Input_str = "";  // 入力された数字
        double Result = 0;      // 計算結果
        string Operator = null; // 押された演算子

        private void button11_Click(object sender, EventArgs e)
        {
            // senderの詳しい情報を取り扱えるようにする
            Button btn = (Button)sender;

            // 押されたボタンの数字(または小数点の記号)
            string text = btn.Text;

            // [入力された数字]に連結する
            Input_str += text;
            // 画面上に数字を出す
            textBox1.Text = Input_str;
        }

        private void button17_Click(object sender, EventArgs e)
        {
            double num1 = Result; // 現在の結果
            double num2;     // 入力された文字


            // 入力された文字が空欄なら、計算をスキップする
            if (Input_str != "")
            {
                // 入力した文字を数字に変換
                num2 = double.Parse(Input_str);

                // 四則計算
                if (Operator == "+")
                    Result = num1 + num2;
                if (Operator == "-")
                    Result = num1 - num2;
                if (Operator == "*")
                    Result = num1 * num2;
                if (Operator == "/")
                    Result = num1 / num2;


                // 演算子を押されていなかった場合、入力されている文字をそのまま結果扱いにする
                if (Operator == null)
                    Result = num2;
            }



            // 画面に計算結果を表示する
            textBox1.Text = Result.ToString() + " cloud";



            // 今入力されている数字をリセットする
            Input_str = "";

            // 演算子をOperator変数に入れる
            Button btn = (Button)sender;
            Operator = btn.Text;

            if (Operator == "=")
                Operator = "";

            if (Result > 0 && Result <= 100 )
            {
                pictureBox1.Image = Properties.Resources.usu1;
                pictureBox2.Image = Properties.Resources.usu2;
                pictureBox3.Image = Properties.Resources.usu3;

                System.Diagnostics.Process.Start("http://www.kumorinochihare.net/kumo-namae.html");
            }
            else if (Result > 100 && Result < 777)
            {

                pictureBox1.Image = Properties.Resources.suji1;
                pictureBox2.Image = Properties.Resources.suji2;
                pictureBox3.Image = Properties.Resources.suji3;

                System.Diagnostics.Process.Start("http://kumoblog.net/category/%E3%81%99%E3%81%98%E9%9B%B2/");
            }
            else if (Result == 777)
            {
                pictureBox1.Image = Properties.Resources.niji1;
                pictureBox2.Image = Properties.Resources.niji2;
                pictureBox3.Image = Properties.Resources.niji3;

                System.Diagnostics.Process.Start("http://naniomo.com/archives/7863");
            }
            else if (Result == 888)
            {
                pictureBox1.Image = Properties.Resources.renzu1;
                pictureBox2.Image = Properties.Resources.renzu2;
                pictureBox3.Image = Properties.Resources.renzu3;

                System.Diagnostics.Process.Start("https://plaza.rakuten.co.jp/yk1430/diary/201406160000/");
            }
            else if (Result > 777 && Result < 888)
            {
                pictureBox1.Image = Properties.Resources.uroko1;
                pictureBox2.Image = Properties.Resources.uroko3;
                pictureBox3.Image = Properties.Resources.uroko2;

                System.Diagnostics.Process.Start("https://weathernews.jp/s/topics/201709/210085/");
            }
            else if (Result > 888 && Result <= 1000)
            {

                pictureBox1.Image = Properties.Resources.iwasi1;
                pictureBox2.Image = Properties.Resources.iwasi3;
                pictureBox3.Image = Properties.Resources.iwasi2;

                System.Diagnostics.Process.Start("https://bokete.jp/boke/68477209");
            }
            else if (Result > 1000 && Result <= 10000)
            {

                pictureBox1.Image = Properties.Resources.hituji1;
                pictureBox2.Image = Properties.Resources.hituji3;
                pictureBox3.Image = Properties.Resources.hituji2;

                System.Diagnostics.Process.Start("https://blue.ap.teacup.com/houjuen/75.html");
            }


           
            else if (Result > 10000 && Result <= 100000)
            {
                pictureBox1.Image = Properties.Resources.nyu1;
                pictureBox2.Image = Properties.Resources.nyu2;
                pictureBox3.Image = Properties.Resources.nyu3;

                System.Diagnostics.Process.Start("https://blogs.yahoo.co.jp/seven7271214/10035162.html");
            }

            else if (Result > 100000 && Result <= 1000000)
            {
                pictureBox1.Image = Properties.Resources.ame1;
                pictureBox2.Image = Properties.Resources.ame2;
                pictureBox3.Image = Properties.Resources.ame;

                System.Diagnostics.Process.Start("https://www.bokka.co.jp/news/archives/13787");
            }
            else if (Result > 1000000 && Result <= 10000000)
            {
                pictureBox1.Image = Properties.Resources.twister1;
                pictureBox2.Image = Properties.Resources.twister2;
                pictureBox3.Image = Properties.Resources.twister3;

                System.Diagnostics.Process.Start("http://athena-minerva.net/sizen/1835/");
            }
            else if (Result > 10000000 && Result <= 100000000)
            {
                pictureBox1.Image = Properties.Resources.yphoon1;
                pictureBox2.Image = Properties.Resources.yphoon2;
                pictureBox3.Image = Properties.Resources.yphoon3;

                System.Diagnostics.Process.Start("https://curazy.com/archives/218283");

            }
            else if (Result > 100000000)
            {
                pictureBox1.Image = Properties.Resources.unkai1;
                pictureBox2.Image = Properties.Resources.unkai2;
                pictureBox3.Image = Properties.Resources.unkai3;

                System.Diagnostics.Process.Start("https://travel.rakuten.co.jp/mytrip/amazing/unkai/");

            }

        }
            private void clearBtn_Click(object sender, EventArgs e)
            {
                Result = 0;
                Input_str = "";
                Operator = null;
                textBox1.Text = Input_str;
                pictureBox1.Image = null;
                pictureBox2.Image = null;
                pictureBox3.Image = null;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://vimeo.com/115877460");
        }

        private void button18_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://vimeo.com/27172301");
        }

        private void button19_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://vimeo.com/29888661");
        }
    }
    }
    
    
    

